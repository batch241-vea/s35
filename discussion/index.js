const express = require('express');

//allows craetion of schemas to model our data structures
// also has access to a number of methods for manipulation on our database
const mongoose = require('mongoose');

const app = express();

const port = 3000;


//[SECTION] - MongoDB Connection
/*	
syntax:
	mongoose.connect("<MongoDB connection string>, {useNewUrlParser: true}")
*/
//connecting mongoDB atlas
//Add password and database name
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.wnkvmfq.mongodb.net/s35?retryWrites=true&w=majority", {
	// due to update in mongoDB drivers that allow connection ro it, the default connection is being flagged as error
	// allows us to avoid any current and future error while connecting to MongoDB
	useNewUrlParser: true, useUnifiedTopology: true
});

//Connecting to MongoDB locally
let db = mongoose.connection;

// responsible for connection error
db.on('error', console.error.bind(console, 'connection error'));

// responsible for successful connection
db.once('open', () => console.log('Connected to MongoDB Atlas!'));


app.use(express.json());

app.use(express.urlencoded({extended: true}));



//[SECTION] Mongoose Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


//[SECTION] Models
// Models must be in singular form and capitalizes
// first parameter - indicates the collection where the data stored
// second parameter - specify the schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);


// Creating a new task
/*
Business Logic
 Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post('/tasks', (req, res) => {
	// to check if there are duplicate tasks
	Task.findOne({name: req.body.name}, (err, result) =>{
		if (result != null && result.name == req.body.name) {
			return res.send('Duplicate task found!');
		} else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				} else{
				return res.status(201).send('New task created');
				};
			})
			
		};
	});
});



//Getting the task
/*
Business Logic
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get('/tasks', (req, res) => {
	Task.find({}, (err, result) => {
		if (err){
			return console.error(err)
		} else {
			return res.status(200).json({
				data: result
			});
		};
	});
});




app.listen(port, () => console.log(`Server running at port: ${port}`));